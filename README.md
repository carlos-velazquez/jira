# Overview
The purpose of this repo is to demonstrate mostly infrastructure provisioning, software deployment and data migration skills.  It was part of a hiring exercise for a prior employer that had the following instructions in their challenge.  
  
1. Deploy any chosen Atlassian software to a publicly accessible location without using any of the provided quick start resources.  
   - And given the 2 week deadline, don't focus on security or a perfect deployment.  
1. Do so in some automated fashion.  
1. (bonus) Import the provided data to the database, make some changes in the system and screenshot it.  
1. Document the steps taken, resources used, screenshots of the running system and any issues/errors encountered along the way.  
  
The challenge was completed without any prior experience deploying Atlassian software.  Some time after being hired the company sponsored me for the ACP-500 (Atlassian Certified System Administrator).  

## Images  
- database_config.png  
- post_import.png  
- customized.png  

## Steps Taken  
1. Search Atlassian docs for any requirements, recommendations and pitfalls  
   - [Jira applications overview](https://confluence.atlassian.com/jiracoreserver/jira-applications-overview-939937971.html)  
   - [Jira applications installation requirements](https://confluence.atlassian.com/adminjiraserver/jira-applications-installation-requirements-938846826.html?_ga=2.110841154.638951787.1541541158-2135373834.1541541158)  
   - [Supported platforms](https://confluence.atlassian.com/adminjiraserver/supported-platforms-938846830.html)  
   - [Connecting Jira applications to a database](https://confluence.atlassian.com/adminjiraserver/connecting-jira-applications-to-a-database-938846850.html)  
   - [Connecting Jira applications to MySQL](https://confluence.atlassian.com/adminjiraserver/connecting-jira-applications-to-mysql-938846854.html)  
   - [Installing additional applications and version updates](https://confluence.atlassian.com/adminjiraserver/installing-additional-applications-and-version-updates-938846848.html)  
   - [JIRA Sizing Guide](https://confluence.atlassian.com/enterprise/jira-sizing-guide-461504623.html)  
   - [Virtualizing JIRA (JIRA on VMware)](https://confluence.atlassian.com/jirakb/virtualizing-jira-jira-on-vmware-461504625.html)  
   - [Setting properties and options on startup](https://confluence.atlassian.com/adminjiraserver073/setting-properties-and-options-on-startup-861253969.html)  
1. Search Atlassian docs for .tar.gz  
   - [Installing Jira applications on Linux from Archive File](https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-on-linux-from-archive-file-938846844.html)  
     - [Jira Core](https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.12.3.tar.gz)  
     - [Jira Software](https://marketplace-cdn.atlassian.com/files/artifact/e03b43ce-383c-4d3e-944b-fbbec24913ad/jira-software-application-7.12.3.obr)  
     - [Jira Service Desk](https://marketplace-cdn.atlassian.com/files/artifact/a2f934ed-437c-49b6-bf51-19549236b36d/jira-servicedesk-application-3.15.3.obr)  
1. Create a CloudFormation template to:  
   - Deploy Amazon AMI on EC2 (recommended min: 2 Cores, 2GB ram, 7200+ RPM HDD)  
   - Deploy MySQL 5.7 RDS DB  
   - Download, install and configure JDK  
   - Download, install, configure and start JIRA  
1. Run the "Set it up for me" wizard to get an eval license (initial idea. see DB issues)    
1. Switch from H2 to MySQL DB (initial idea. see DB issues)  
   - [Switching Databases](https://confluence.atlassian.com/adminjiraserver/switching-databases-938846867.html)  
1. Import sample data  
   - Edit the files to point to my instance's url, file paths, etc by comparing with my own export and re-zip  
1. Customize  

## Issues  
1. Cloudformation template validation error on upload. (DB password regex also needs `[` escaped)  
   - Dug into the Cloudformation guide and saw it mention a Java parser  
   - Visited a Java regex test site to find the issue with the non escaped open bracket  
1. Cloudformation template validation error on execution. ("The parameter groupName cannot be used with the parameter subnet" when specifying SecurityGroup + SubnetId)  
   - Googled a bit. Didn't find a satisfactory answer, but a hint at what the issue is  
   - Dug into the Cloudformation guide's Security Groups and GetAtt sections and found a way to make it work  
1. Cloudformation template validation error on upload. (DependsOn property for EC2 instance in the wrong spot)  
   - Corrected it  
1. Java crashes due to OOM. (Knew it likely would given the minimum specs, but hoped it wouldn't)  
   - Created a swap file as AWS has my account limits restricted to micro instance types  
1. Communication failure to the DB on testing connection after redeploying stack with utf-8 charset  
   - Was working the first time I tested it via "I'll Set It Up Myself".  Now isn't  
   - Seeing `Error connection to database" "Communications link failure The last packet sent successfully to the server was 0 milliseconds ago. The driver has not received any packets from the server.`  
   - catalina.out showing `java.net.UnknownHostException: ... name or service not known`  
   - nslookup of AWS DNS endpoint from server works  
   - mysql CLI connection from server works  
   - Added a /etc/hosts entry and restarted  Jira. not working  
   - Double checked MySQL driver 5.1 was installed  
   - Double checked .jar in the installation /lib folder  with correct owner/mode permissions  
   - Redeployed the stack with the default DB parameter group (latin1 charset). not working  
   - Redeployed the stack with my custom DB parameter group (utf8 charset). not working  
   - Manually created dbconfig.xml with MySQL entries and restarted Jira. working?  
   - Checked  "Database monitoring" in the System section. looks like it's working  
   - Connected to the RDS DB via CLI and checked tables in `jiradb`. definitely working  
   - Moved dbconfig.xml out of the Jira home folder and restarted Jira  
   - Even with an /etc/hosts entry the test in "I'll Set It Up Myself" failed  
   - Removed /etc/hosts entry, put dbconfig.xml back in the home folder and restarted Jira. working again  
   - Added dbconfig.xml to the Cloudformation template  
   - Destroyed and redeployed the stack to make extra sure  
1. Can't log in after the provided sample DB export was imported  
   - Changed the admin user password hash in the DB by generating a new one with the passlib python library  

## Next Steps  
1. Secure the connection with SSL (ELB/HAProxy w/ termination)  
2. Service Monitoring (CloudWatch, Nagios)  
3. Alerting (CloudWatch, Op5)  
4. High Availability? (ELB)  
